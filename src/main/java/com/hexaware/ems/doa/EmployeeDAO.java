package com.hexaware.ems.doa;

import java.util.Set;

import com.hexaware.ems.model.Employee;

public interface EmployeeDAO {
	
	public Employee save(Employee employee);
	
	public Set<Employee> findAll();
	
	public Employee findById(long employeeId);
	
	public void deleteById(long employeeId);
	
	
	
}
